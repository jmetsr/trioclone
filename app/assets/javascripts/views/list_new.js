TrelloClone.Views.List = Backbone.View.extend({
  template: JST["lists/new"],
  tagname: "form",
  initialize: function(options){
    this.render();
  },
  render: function(){
    this.model = new TrelloClone.Models.List();
    if (typeof current_board_id === "undefined"){
      current_board_id = 9;
    }
    this.model.board_id = current_board_id;
    var templ = this.template();
    this.$el.html(templ);
    return this;
  },
  events: {
    "click button":"submitList"
  },
  submitList: function(event){
    event.preventDefault();
    var formData = this.$el.serializeJSON();
    var model = new TrelloClone.Models.List(formData);
    model.board_id = current_board_id;
    model.save();
    alert("saved successfully");
  }

})