TrelloClone.Views.BoardsIndex = Backbone.View.extend({

  initialize: function(options){
    this.listenTo(this.collection, "sync", this.render);
  },

  template: JST["boards/index"],

  render: function(){
    c = this.collection;
    var templ = this.template({boards: this.collection});
    this.$el.html(templ);
    return this;
  },
})
