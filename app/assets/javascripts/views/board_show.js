TrelloClone.Views.BoardShow = Backbone.View.extend({
  template: JST["boards/show"],
  render: function(){

    var templ = this.template({
      board: this.board,
      lists: this.lists
     })
    this.$el.html(templ);
    return this;
  },

  initialize: function(options){
   this.board = options.board;
   this.lists = options.lists;
   this.listenTo(this.board, "sync", this.render);
   this.listenTo(this.lists, "sync", this.render);
   this.board.fetch();
   this.lists.fetch();
   current_board_id = this.board.id
//   this.lists = this.board.lists().fetch();
  // this.lists = options.lists
   }
})