TrelloClone.Views.Board = Backbone.View.extend({
  template: JST["boards/new"],
  tagName: "form",

  render: function(){
    this.model = new TrelloClone.Models.Board();
    var templ = this.template({board: this.model});
    this.$el.html(templ);
    return this;
  },

  initialize: function(options){
    this.render();
  },

  events: {
    "click button":"submitBoard"
    //"click buttom":"navigate"
  },

  submitBoard: function(event){
    event.preventDefault();
    var formData = this.$el.serializeJSON();

    var model = new TrelloClone.Models.Board(formData);
    model.save({},{
       success: function(){
   //     TrelloClone.Routers.AppRouter.boardShow(model.get("id"));
     console.log(model);
        Backbone.history.navigate(
          "boards/"+ model.id,
          {trigger: true}
        );
      },error: function(){
        console.log("error")
      }
    })
  }

  //navigate: function(event){
    //event.preventDefault();
    //var url = this.$el.serializeJSON()
    //}
})