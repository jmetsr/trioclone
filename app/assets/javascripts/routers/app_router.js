TrelloClone.Routers.AppRouter = Backbone.Router.extend({

  routes: {
    "": "boardsIndex",
    "boards/new": "BoardsNew",
    "boards/:id": "boardShow",
    "lists/new": "listNew"
  },


  boardsIndex: function(){
    TrelloClone.Collections.boards.fetch({
      success: function(){
        var boardsIndex = new TrelloClone.Views.BoardsIndex({
          collection: TrelloClone.Collections.boards
        });
        boardsIndex.render()
        $('.body').html(boardsIndex.$el);
      }
    })
  },

  BoardsNew: function(){
    var newView = new TrelloClone.Views.Board();
    newView.render();
    $(".body").html(newView.$el);
  },

  boardShow: function(id){
    TrelloClone.Collections.boards.fetch({
      success: function(){
        var board = TrelloClone.Collections.boards.getOrFetch(id);
        var showView = new TrelloClone.Views.BoardShow({
          board: board,
          lists: board.lists()
        });

        showView.render();
        $(".body").html(showView.$el);
      }
    });
  },

  listNew: function(){
    var newList = new TrelloClone.Views.List();
    newList.render();
    $('body').html(newList.$el);
  }
})