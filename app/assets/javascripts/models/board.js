TrelloClone.Models.Board = Backbone.Model.extend({
  urlRoot: "/api/boards",
  lists: function(){
    if (!this._lists){
      this._lists = new TrelloClone.Collections.BoardLists([],{
        board: this
      });
    }
    return this._lists
  },

  parse: function(data){
    if (data.lists)
    {
      var boardLists = this.lists();
      boardLists.set(data.lists, {parse: true});
      delete data.lists;
    }
    return data;
  }
})
