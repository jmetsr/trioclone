TrelloClone.Collections.Boards = Backbone.Collection.extend({
  model: TrelloClone.Models.Board,
  url: "api/boards"
})

TrelloClone.Collections.Boards.prototype.getOrFetch = function(id){
  model = this.get(id);
  if (typeof model !== "undefined"){
    model.fetch();
  }

  return model;
}

TrelloClone.Collections.boards = new TrelloClone.Collections.Boards();

