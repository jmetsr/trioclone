TrelloClone.Collections.BoardLists = Backbone.Collection.extend({
  model: TrelloClone.Models.List,
  url: function(){
    return "/api/lists";
  },
  initialize: function(models, options){
    this.board = options.board;
  }
})